# pycowsay-rich

A talking cow! A [rich](https://github.com/Textualize/rich) version of [pycowsay](https://github.com/cs01/pycowsay).

## Run

Use [pipx](https://pypa.github.io/pipx/) to run without permanently installing.

```bash
pipx run pycowsay-rich mooo
```

## Install

Use [pipx](https://pypa.github.io/pipx/) to install to isolated enviroment.

```bash
pipx install pycowsay-rich
```

Use [pip](https://pip.pypa.io/en/stable/) to install to python enviroment.

```bash
pip install pycowsay-rich
```

